import java.util.*;

/**
 * @author EEMAOOV
 *
 */
public class GraphTask {
   ArrayList nodes = new ArrayList();


   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   public void run() {

      Graph g = new Graph ("G");
      g.createRandomSimpleGraph (6, 9);
      int eccentricity = g.breadthFirstSearch(g.first);
      System.out.println("This graph eccentricity is: " + eccentricity);

      // TODO!!! Your experiments here
   }


  static public class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      private int distance;

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }
      
      /**
     * @param first Arc
     */
      public void setFirstArc(Arc first){
    	  this.first = first;
      }
      
      /**
     * @param next Vertex
     */
      public void setNextVertex(Vertex next){
    	  this.next = next;
      }
      
      /**
     * @return next vertex
     */
    public Vertex getNextVertex()
      {
         return next;
      }


      /**
      *
      * @return given vertex Arcs
      */
    public ArrayList<Arc> getVertexArcList() {
    	  
          ArrayList<Arc> arcs = new ArrayList();
          Arc arc = first;
          arcs.add(arc);
          
          while (arc.next != null) {
            arc = arc.next;
            arcs.add(arc);
          }
          
          return arcs;
      }

    	
    
       /**
       * 
     * @param vertex "color" information
     */
    public void settVertexVisited(int info) {
          this.info = info;
       }


      /**
     * @return vertex "color" information
     */
    public int getVertexVisited() {
          return info;
      }


      /**
       * Sets vertex distance
     * @param distances (steps from starting vertex)
     */
    public void setDistance(int distance){
         this.distance = distance;
      }

    
      /**
     * @return vertex distance (steps) from starting vertex
     */
    public int getDistance(){
          return distance;

}

}

   static class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;

      Arc(String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }


      Arc(String s) {
         this(s, null, null);
     }

      @Override
      public String toString() {
         return id;
       }
      
      /**
     * @param next Arc
     */
    public void setNextArc(Arc next){
    	  this.next = next;
      }
      

      /**
     * @param target Vertex
     */
    public void setTargetVertex(Vertex target){
    	  this.target = target;
      }
      
      
      /**
     * @return target vertex
     */
    public Vertex getTargetVertex() {
         return target;
      }

    }

    static class Graph {

        private String id;
        private Vertex first;
        private int info = 0;

        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph(String s) {
            this(s, null);
        }
        
        /**
         * @param a Vertex
         */
        public void setFirst(Vertex a){
        	first = a;
        }

        /**
         * @return first Vertex
         */
        public Vertex getFirst() {
            return first;
        }

        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuffer sb = new StringBuffer(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.toString());
                    sb.append("->");
                    sb.append(a.target.toString());
                    sb.append(")");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        public Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            return res;
        }

        public Arc createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
        public void createRandomSimpleGraph(int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
            first = null;
            createRandomTree(n); // n-1 edges created here
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1; // remaining edges
            while (edgeCount > 0) {
                int i = (int) (Math.random() * n); // random source
                int j = (int) (Math.random() * n); // random target
                if (i == j)
                    continue; // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue; // no multiple edges
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected[i][j] = 1;
                createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                connected[j][i] = 1;
                edgeCount--; // a new edge happily created
            }
        }

        
     
        /**
         * BreadthFirstSearch algorithms 
         * Used sources: http://interactivepython.org/courselib/static/pythonds/Graphs/ImplementingBreadthFirstSearch.html
         * 				http://kaygun.tumblr.com/post/37827373953/eccentricity-radius-and-diameter-in-an-undirected
         * @param Given starting point(vertex) in Graph
         * @return Graph eccentricity
         */
        public int breadthFirstSearch(Vertex start) {

            if (!getThisGraphVertexList().contains(start))
                throw new RuntimeException("This vertex is not in the graph");
            if (getThisGraphVertexList().isEmpty())
            	throw new RuntimeException("There must be at least one vertex in graph");
            if (getThisGraphVertexList().size() == 1)
            	return 0;
            if (start.getVertexArcList().size() < 1)
                return 0;
            
            List<Integer> distTo = new ArrayList();
        	int eccentricity = 0;
            this.setGraphInit();
            int m = 0;
            start.setDistance(0);
            Queue queue = new LinkedList();
            queue.add(start);
            start.settVertexVisited(1); // visited
            while (queue.size() > 0) {              
                Vertex v = (Vertex) queue.poll(); // FIFO
                ArrayList<Arc> arcs = v.getVertexArcList();
                for(Iterator<Arc> i = arcs.iterator(); i.hasNext();)                
                {
                   Arc curArc = i.next();
                   Vertex curVertex = curArc.getTargetVertex();
                   if(curVertex.getVertexVisited() == 0) //Vertex not visited
                   {
                       curVertex.setDistance(v.getDistance() + 1);
                       distTo.add(curVertex.getDistance());
                       queue.add(curVertex);
                       curVertex.settVertexVisited(1);
                   }
                    
                }
                v.settVertexVisited(2); // traveled through
            }
            
            // calculate eccentricity
            for (int i = 0; i < distTo.size(); i++) {
    			if(distTo.get(i) > eccentricity)
    				eccentricity = (int)distTo.get(i);
    		}
            
		    return eccentricity;
            
       }


        /**
         * All Graph vertexes
         * @return Graph vertexes list
         */
        private ArrayList<Vertex> getThisGraphVertexList() {
        	
            ArrayList<Vertex> l = new ArrayList<Vertex>();
            Vertex v = first;
            l.add(v);
            
            while (v.next != null) 
            {
                l.add(v.getNextVertex());
                v = v.getNextVertex();
            }
            return l;
        }


        /**
         * Graph vertexes initializing
         * All vertexes are set unvisited
         */
        public void setGraphInit() {
            ArrayList<Vertex> vl = getThisGraphVertexList();
            for (int i = 0; i < vl.size(); i++) 
            {
                vl.get(i).settVertexVisited(0);
            }
        }

    }
} 

