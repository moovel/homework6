import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author EEMAOOV
 *
 */
public class GraphTaskTest {

	
	@Test (timeout=10000)
    public void test1() {
		GraphTask.Graph g = new GraphTask.Graph("G");
        g.createRandomSimpleGraph(1,0);
        assertEquals(g.breadthFirstSearch(g.getFirst()), 0);

    }

    @Test (timeout=10000)
   public void test2() {
    	GraphTask.Graph g = new GraphTask.Graph("G");
        GraphTask.Vertex a = new GraphTask.Vertex("A");
        GraphTask.Vertex b = new GraphTask.Vertex("B");

        g.setFirst(a);
        a.setNextVertex(b);

        GraphTask.Arc ab = new GraphTask.Arc("AB");
        GraphTask.Arc ba = new GraphTask.Arc("BA");

	    a.setFirstArc(ab);
	    ab.setTargetVertex(b);
	    
	    b.setFirstArc(ba);
        ba.setTargetVertex(a);
        
        assertEquals(g.breadthFirstSearch(g.getFirst()), 1);

    }

    @Test (timeout=10000, expected=RuntimeException.class)
    public void test3() {
    	GraphTask.Graph g = new GraphTask.Graph("G");
        g.createRandomSimpleGraph(0,0);
        assertEquals(g.breadthFirstSearch(g.getFirst()), 0);

    }

    @Test (timeout=10000)
    public void test4() {
        GraphTask.Graph g = new GraphTask.Graph("G");
        GraphTask.Vertex a = new GraphTask.Vertex("A");
        GraphTask.Vertex b = new GraphTask.Vertex("B");
        GraphTask.Vertex c = new GraphTask.Vertex("C");
        GraphTask.Vertex d = new GraphTask.Vertex("D");

       g.setFirst(a);
       a.setNextVertex(b);
       b.setNextVertex(c);
       c.setNextVertex(d);



       GraphTask.Arc ab = new GraphTask.Arc("AB");
       GraphTask.Arc ba = new GraphTask.Arc("BA");

       GraphTask.Arc ac = new GraphTask.Arc("AC");
       GraphTask.Arc ca = new GraphTask.Arc("CA");

       GraphTask.Arc bd = new GraphTask.Arc("BD");
       GraphTask.Arc db = new GraphTask.Arc("DB");

       GraphTask.Arc cd = new GraphTask.Arc("CD");
       GraphTask.Arc dc = new GraphTask.Arc("DC");

      a.setFirstArc(ab);
      ab.setTargetVertex(b);
      ab.setNextArc(ac);
      ac.setTargetVertex(c);

      b.setFirstArc(ba);
      ba.setTargetVertex(a);
      ba.setNextArc(bd);
      bd.setTargetVertex(d);

      c.setFirstArc(ca);
      ca.setTargetVertex(a);
      ca.setNextArc(cd);
      cd.setTargetVertex(d);

      d.setFirstArc(db);
      db.setTargetVertex(b);
      db.setNextArc(dc);
      dc.setTargetVertex(c);
      assertEquals(g.breadthFirstSearch(g.getFirst()), 2);
      
    }
    
    
    @Test (timeout=10000)
    public void test5() {
        GraphTask.Graph g = new GraphTask.Graph("G");
        GraphTask.Vertex a = new GraphTask.Vertex("A");
        GraphTask.Vertex b = new GraphTask.Vertex("B");
        GraphTask.Vertex c = new GraphTask.Vertex("C");
        GraphTask.Vertex d = new GraphTask.Vertex("D");


      g.setFirst(a);
      a.setNextVertex(b);
      b.setNextVertex(c);
      c.setNextVertex(d);


      GraphTask.Arc ab = new GraphTask.Arc("AB");
      GraphTask.Arc ba = new GraphTask.Arc("BA");

      GraphTask.Arc ac = new GraphTask.Arc("AC");
      GraphTask.Arc ca = new GraphTask.Arc("CA");

      GraphTask.Arc ad = new GraphTask.Arc("AD");
      GraphTask.Arc da = new GraphTask.Arc("DA");

      GraphTask.Arc bc = new GraphTask.Arc("BC");
      GraphTask.Arc cb = new GraphTask.Arc("CB");

      GraphTask.Arc bd = new GraphTask.Arc("BD");
      GraphTask.Arc db = new GraphTask.Arc("DB");

      GraphTask.Arc cd = new GraphTask.Arc("CD");
      GraphTask.Arc dc = new GraphTask.Arc("DC");


      a.setFirstArc(ab);
      ab.setTargetVertex(b);
      ab.setNextArc(ac);
      ac.setTargetVertex(c);
      ac.setNextArc(ad);
      ad.setTargetVertex(d);

      b.setFirstArc(ba);
      ba.setTargetVertex(a);
      ba.setNextArc(bc);
      bc.setTargetVertex(c);
      bc.setNextArc(bd);
      bd.setTargetVertex(d);

      c.setFirstArc(ca);
      ca.setTargetVertex(a);
      ca.setNextArc(cb);
      cb.setTargetVertex(b);
      cb.setNextArc(cd);
      cd.setTargetVertex(d);

      d.setFirstArc(da);
      da.setTargetVertex(a);
      da.setNextArc(db);
      db.setTargetVertex(b);
      db.setNextArc(dc);
      dc.setTargetVertex(c);
      assertEquals(g.breadthFirstSearch(g.getFirst()), 1);

    }

}
